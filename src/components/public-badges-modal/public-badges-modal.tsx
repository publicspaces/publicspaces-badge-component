import { Component, Prop, State, Event, EventEmitter, h } from "@stencil/core";
import { ModalPositioning } from "../../interfaces";
import { IconClose, IconMore } from "../../assets/icons";
import { Metadata } from "../../assets/metadata";


@Component({
  tag: "publicbadges-modal",
  styleUrl: "public-badges-modal.scss",
})

export class PublicbadgesModal {
  modalEl!: HTMLElement;

  @Prop() public theme: string = "";
  @Prop() public language: "EN" | "NL" = "EN";
  @Prop() public positioning: ModalPositioning = { orientation: "vertical", left: 0, origin: "top" };

  @State() public openBadge: number | null = null

  @Event() closeDrawer!: EventEmitter;
  closeDrawerHandler() {
    this.closeDrawer.emit(false);
  }

  public componentDidLoad() {
    if(this.positioning.orientation === "vertical") {
      window.scrollBy(0, this.modalEl.getBoundingClientRect().top - 10)
    }
  }

  public closeModalHandler = () => {
    this.closeDrawer.emit();
  }


  public render() {
    const metadata = Metadata[this.language];
    const { orientation, left, origin } = this.positioning
    const modalColorBg = this.theme === "dark" ? "#3C3C3C" : "#FFF";
    const modalColorFg = this.theme === "dark" ? "#FFF" : "#3C3C3C";

    const domainName = window.location.hostname.split('.').slice(-2).join(".")

    return (
      [
        <div id="modal-bg" onClick={ this.closeModalHandler }></div>,
        <div id="modal" class={`${orientation} ${origin}`} style={{ left: left.toString()+"px", "--modal-color-bg": modalColorBg, "--modal-color-fg": modalColorFg}} ref={(el) => this.modalEl = el as HTMLElement}>
          <button class="close" onClick={ this.closeModalHandler }>
            <IconClose />
          </button>
          <div id="modal-content">
            <div id="logo" class="column">
              <publicbadges-circle interactive={false}></publicbadges-circle>
            </div>
            <div id="about" class="column">
              <h1>{metadata.title}</h1>
              <h2>{metadata.subtitle}</h2>
              <div innerHTML={metadata.about}></div>
              <p>
                <a class="more" href={`${metadata.baseURL}?r=${domainName}`} target="_blank" rel="noopener noreferrer">
                  {metadata.more}
                  <IconMore />
                </a>
              </p>
            </div>
          </div>
        </div >
      ]
    );
  }
}
