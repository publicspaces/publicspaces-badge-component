import { Component, Element, Prop, State, Listen, Host, h } from "@stencil/core";
import { ModalPositioning } from "../../interfaces";
import { calculateModalPositioning, addFont } from "../../utils/utils";


@Component({
  tag: "publicbadges-drawer",
  styleUrl: "public-badges-drawer.scss",
  shadow: true
})

export class PublicbadgesDrawer {
  @Element() public el: HTMLElement | undefined;

  @Prop() public fontEndpoint: string = "https://assets.publicspaces.net/@publicbadges/font/";

  @Prop() public badgeColor: string = "#3C3C3C";
  @Prop() public modalTheme: "dark" | "light" = "light";
  @Prop() public language: "EN" | "NL" = "EN";

  @State() public fontLoaded: boolean = false;
  @State() public isOpen: boolean = false;
  @State() public modalPositioning: ModalPositioning = { orientation: "vertical", left: 0, origin: "top" };


  // Handlers
  public openDrawer = () => {
    if(this.el) {
      this.modalPositioning = calculateModalPositioning(this.el);
    }
    if(!this.fontLoaded) {
      addFont(this.fontEndpoint);
      this.fontLoaded = true;
    }
    this.isOpen = true;
  };

  public handleMouseEnter = () => {
    if(!this.fontLoaded) {
      addFont(this.fontEndpoint);
      this.fontLoaded = true;
    }
  }

  @Listen('keydown', { target: 'window' })
  handleKeyDown(ev: KeyboardEvent){
    if(ev.key === "Escape") {
      this.isOpen = false;
    }
  }

  @Listen("closeDrawer")
  public closeDrawer() {
    this.isOpen = false;
  }

  @Listen('resize', { target: 'window' })
  handleWindowResize(){
    if(this.el) {
      this.modalPositioning = calculateModalPositioning(this.el);
    }
  }


  // Render
  public render() {
    return (
      <Host style={{ zIndex: this.isOpen ? "9999" : "1" }} onMouseEnter={()=>{ this.handleMouseEnter() }}>
        <publicbadges-circle
          badgesCount={1}
          interactive={this.isOpen ? false : true}
          onClick={ this.openDrawer }
          style={{ "--badge-color": this.badgeColor }}>
        </publicbadges-circle>
        { this.isOpen &&
          <publicbadges-modal
            theme={this.modalTheme}
            language={this.language}
            positioning={this.modalPositioning}>
          </publicbadges-modal> }
      </Host>
    );
  }
}
