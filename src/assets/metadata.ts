
export const Metadata: any = {
  EN: {
    baseURL: 'https://badge.publicspaces.net',
    title: 'PublicSpaces',
    subtitle: 'internet for the common good',
    about: `<p>PublicSpaces reclaims the internet as a force for the common good and advocates a new internet that strengthens the public domain.</p>
      <p>With this badge we show our commitment to the values of the <a href="https://publicspaces.net/manifesto" target="_blank" rel="noopener noreferrer">Public Spaces manifesto</a>.</p>
      <p>More information about the tools we use can be found in our PublicSpaces PowerWash.</p>`,
    more: 'See our PowerWash'
  },
  NL: {
    baseURL: 'https://badge.publicspaces.net',
    title: 'PublicSpaces',
    subtitle: 'internet in het publieke belang',
    about: `<p>PublicSpaces staat voor het publieke belang op het internet en ondersteunt internet-technologie die het publieke domein versterkt.</p>
      <p>Met deze badge laten we zien dat wij ons verbinden met de waarden van het <a href="https://publicspaces.net/manifesto" target="_blank" rel="noopener noreferrer">PublicSpaces manifest</a>.</p>
      <p>Meer informatie over de tools die wij hierbij gebruiken is te vinden in onze PublicSpaces Spoelkeuken.</p>`,
    more: 'Naar de Spoelkeuken'
  }
}
